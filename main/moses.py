import numpy as np
import torch
import cv2
from glob import glob
import os.path as osp
import os
import a_geometry as geo

from config import cfg

import smallestenclosingcircle

np.set_printoptions(precision=3, suppress=True)

# from utils.preprocessing import load_img, load_skeleton, get_bbox, process_bbox, augmentation, transform_input_to_output_space, trans_point2d
from utils.transforms import world2cam, cam2pixel, cam2pixel_depth
# from utils.vis import vis_keypoints, vis_3d_keypoints
import random
import ujson as json
import math
import pandas as pd

img_path = '../data/InterHand2.6M/images'
annot_path = '../data/InterHand2.6M/annotations'


def wobble_predictions_3d(predictions_3d):
  hand_size = np.linalg.norm(predictions_3d[0]-predictions_3d[9]) # distance from wrist to middle-pxm
  print(hand_size)


  move_all_fac = 0.3
  move_individual_fac = 0.02
  # (21, 3)
  move_all = np.ones((21, 3)) * np.random.normal(0, hand_size*move_all_fac, 3)

  move_individual = np.random.normal(0, hand_size*move_individual_fac, (21, 3))

  ret = predictions_3d + move_all + move_individual

  print("before wobble\n", ret, "after wobble\n",  predictions_3d)

  return ret



class InterDataset(torch.utils.data.Dataset):
  # root_dir = 
  def __init__(self):
    print("hi!");
    self.joints = {"train": [None]*26, "test": [None]*26, "val": [None]*26}
    self.cams = {}
    for mode in "train", "test", "val":
      with open(osp.join(annot_path, mode, 'InterHand2.6M_' + mode + '_camera.json')) as f:
        self.cams[mode] = json.load(f)
    
  def to_single_hand_order(self, joint_coord, hand_idx):
    new_joint_coord = np.empty((21, 3))
    if hand_idx == 0:
      joint_coord = joint_coord[0:21]
    else:
      joint_coord = joint_coord[21:]
    # Wrist.
    new_joint_coord[0] = joint_coord[20]
    for finger in range(5):
      offs = finger*4
      new_joint_coord[1+offs] = joint_coord[3+offs]
      new_joint_coord[2+offs] = joint_coord[2+offs]
      new_joint_coord[3+offs] = joint_coord[1+offs]
      new_joint_coord[4+offs] = joint_coord[0+offs]
    return new_joint_coord
  
  def fingertips_to_sphere_centers(self, joints):
    lerp_amount = 0.3
    for finger in range(5):
      offs_tip = 1 + finger*4 + 3
      offs_dst = 1 + finger*4 + 2

      tip = joints[offs_tip]
      dst = joints[offs_dst]


      joints[offs_tip] = (tip * (1.0-lerp_amount)) + (dst * lerp_amount)

  def predict_last_two(self, joint_n_2, joint_n_1):

    # return (joint_n_1*2) - joint_n_2 # ???? really? don't trust yourself.

    dir = joint_n_1 - joint_n_2

    out = joint_n_1 + dir


    return out
  
  

  def to_cam_coord(self, cameras, capture_id, cam, joints, hand_idx):
    campos, camrot = np.array(cameras[str(capture_id)]['campos'][str(cam)], dtype=np.float32), np.array(cameras[str(capture_id)]['camrot'][str(cam)], dtype=np.float32)
    focal, princpt = np.array(cameras[str(capture_id)]['focal'][str(cam)], dtype=np.float32), np.array(cameras[str(capture_id)]['princpt'][str(cam)], dtype=np.float32)
    # print(joints[str(capture_id)].keys())
    joint_world = np.array(joints['world_coord'], dtype=np.float32)
    joint_world = self.to_single_hand_order(joint_world, hand_idx)
    self.fingertips_to_sphere_centers(joint_world)
    joint_cam = world2cam(joint_world.transpose(1,0), camrot, campos.reshape(3,1)).transpose(1,0) 
    return joint_cam

  

  def to_px_coord(self, cameras, capture_id, cam, joint_cam, hand_idx):
    # campos, camrot = np.array(cameras[str(capture_id)]['campos'][str(cam)], dtype=np.float32), np.array(cameras[str(capture_id)]['camrot'][str(cam)], dtype=np.float32)
    print("to_px_coord ???")
    focal, princpt = np.array(cameras[str(capture_id)]['focal'][str(cam)], dtype=np.float32), np.array(cameras[str(capture_id)]['princpt'][str(cam)], dtype=np.float32)
    # # print(joints[str(capture_id)].keys())
    # joint_world = np.array(joints['world_coord'], dtype=np.float32)
    # joint_world = self.to_single_hand_order(joint_world, hand_idx)
    # self.fingertips_to_sphere_centers(joint_world)
    # joint_cam = self.to_cam_coord(cameras, capture_id, cam, joints, hand_idx)
    joint_img = cam2pixel_depth(joint_cam, focal, princpt)

    # joint_coord = np.concatenate((joint_img, joint_cam[:,2,None]),1)

    # print("joint_img ->\n", joint_img)
    return joint_img

    
  def __getitem__(self, idx):
    file = f"/media/moses/TRAINDATA/hd/{idx}"

    a = pd.read_csv(file, delimiter=" ", quotechar="|", header=None)
    b = a.iloc[0]
    # hand_idx, mode, 
    # [0, 'test', 0, 'ROM01_No_Interaction_2_Hand', 400262, 20625, True]

    hand_idx = int(b[0])
    mode = str(b[1])
    capture_idx = int(b[2]) # 0 to 26 for train
    name = str(b[3])
    cam_id = int(b[4])
    frame_idx = int(b[5])
    frame_idx_n_1 = int(b[6])
    frame_idx_n_2 = int(b[7])

    # BAD
    if (hand_idx == 0):
      return
    
    # has_prior_hands = bool(b[6])

    # Lazy load
    if (self.joints[mode][capture_idx] == None):

      print("2")
      joint_name = osp.join(annot_path, mode, 'InterHand2.6M_' + mode + f'_joint_3d{capture_idx}.json')
      with open(joint_name) as f:
        joints = json.load(f)
      
      self.joints[mode][capture_idx] = joints

    img_file = osp.join(img_path, mode, f"Capture{capture_idx}", name, f"cam{cam_id}", f"image{frame_idx}.jpg")
    # if img_file == None:

    joints_curr = self.to_cam_coord(self.cams[mode], capture_idx, cam_id, self.joints[mode][capture_idx][str(frame_idx)], hand_idx)
    # print(joints_curr)

    img = cv2.imread(img_file, cv2.IMREAD_GRAYSCALE)
    img = img.astype(np.float32) * 1.0/255.0
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    # if 

    print(frame_idx_n_2, frame_idx_n_1, frame_idx)

    n_1 = self.to_cam_coord(self.cams[mode], capture_idx, cam_id, self.joints[mode][capture_idx][str(frame_idx_n_1)], hand_idx)
    n_2 = self.to_cam_coord(self.cams[mode], capture_idx, cam_id, self.joints[mode][capture_idx][str(frame_idx_n_2)], hand_idx)

    predicted = wobble_predictions_3d(self.predict_last_two(n_2, n_1))
    # predicted = self.predict_last_two(n_2, n_1)

    # colors = (1, 0, 1), (0, 1, 0)
    colors = (1, 0, 0), (0, 1, 0), (0, 0, 1), (1, 0, 1)
    
    for idx, j in enumerate((predicted, joints_curr)):
      print("before: \n", j)
      joint_coord = self.to_px_coord(self.cams[mode], capture_idx, cam_id, j,  hand_idx)
      print("after: \n", j)

      for coord in joint_coord:
        cv2.circle(img, (int(coord[0]), int(coord[1])), 2, colors[idx])
    
      for finger in range(5):
        last_idx = 0
        for joint in range(4):
          coord = joint_coord[last_idx]
          prev = (int(coord[0]), int(coord[1]))

          curr_idx = 1 + finger*4 + joint
          
          coord = joint_coord[curr_idx]
          next = (int(coord[0]), int(coord[1]))
          last_idx = curr_idx;


          # cv2.circle(img, (int(coord[0]), int(coord[1])), 2, (1, 0, 1))

          cv2.line(img, prev, next, colors[idx])


    # print(f)


    predicted_px = self.to_px_coord(self.cams[mode], capture_idx, cam_id, predicted,  hand_idx)
    gt_px = self.to_px_coord(self.cams[mode], capture_idx, cam_id, joints_curr,  hand_idx)

    print("predicted px -> ", predicted_px[:, :2])
    x, y, r = smallestenclosingcircle.make_circle(predicted_px[:, :2])
    x, y, r = geo.minicircle(predicted_px[:, :2])
    print(x,y,r)
    # exit(0)
    # cv2.circle(img, (int(x), int(y)), r, colors[0])

    is_right = True
    final_r = r * 1.3
    center = np.array((x, y))
    sz = 512
    in_mat = img


    rot = random.uniform(-math.pi, math.pi) 

    x_axis = np.float32([math.cos(rot), math.sin(rot)])*final_r
    if is_right:
        x_axis *= -1
    y_axis = np.float32([-math.sin(rot), math.cos(rot)])*final_r

    tl = center - x_axis - y_axis
    tr = center + x_axis - y_axis
    bl = center - x_axis + y_axis

    tl_o = (0, 0)
    tr_o = (sz, 0)
    bl_o = (0, sz)

    src_tri = np.float32((tl, tr, bl))
    dst_tri = np.float32((tl_o, tr_o, bl_o))

    trans = cv2.getAffineTransform(src_tri, dst_tri)

    transformedHand = cv2.warpAffine(in_mat, trans, (sz, sz))

    print(x,y,r)

    cv2.imshow("hi", transformedHand)
    cv2.waitKey(0)


    # print(a.iloc[0])
    return None


d = InterDataset()


for e in d:
  print(e)

for i in range(100, 500):
  print(d[i])
  # print("")

# d[0]
# d[1]
# d[7]
