import json
import numpy as np
import json
import os
import time
import cv2
import matplotlib
import random
import math

def mat_float32touint8(mat):
    return (mat*255).astype(np.uint8)


def mat_uint8tofloat32(mat):
    return mat.astype(np.float32)*1.0/255.0


def interize(b):
    return (int(b[0]), int(b[1]))


def transformVecBy2x3(input, M):
    thing = input.copy()
    thing[0] = (input[0] * M[0, 0]) + (input[1] * M[0, 1]) + M[0, 2]
    thing[1] = (input[0] * M[1, 0]) + (input[1] * M[1, 1]) + M[1, 2]

    return thing


def in_line(x, p1, p2):
    return ((p2[1]-p1[1])/(p2[0]-p1[0]))*(x-p1[0])+p1[1]

def piecewise(x, pts_list):
  for i in range(len(pts_list)):
    if (x > pts_list[i][0]) and (x < pts_list[i+1][0]):
      return in_line(x, pts_list[i], pts_list[i+1])
  raise RuntimeError("Input not in list of points!")



def arbitrary_density(x, val_and_density_list):
    curr_pt = 0
    old_curr_pt = 0
    found = False
    
    for i in range(len(val_and_density_list)-1):
        old_curr_pt = curr_pt
        curr_pt += val_and_density_list[i][1]
        # print(x, curr_pt, (x < curr_pt))
        if (x < curr_pt):
            # print("hi!")
            the_val = in_line(x, (old_curr_pt, val_and_density_list[i][0]), (curr_pt, val_and_density_list[i+1][0]))
            return the_val

    raise RuntimeError("Input not in list of points!")


def draw_hand_rainbow_pts(img, pts):
  for j in range(21):
    rgb = matplotlib.colors.hsv_to_rgb([j/21.0,1.0,1.0])
    for ele in rgb:
      ele *= 255.0
    cv2.circle(img, (int(pts[j][0]), int(pts[j][1])), 5, (rgb[2], rgb[1], rgb[0]), cv2.FILLED)

def calc_num_times_to_make_image(requested_num_images, num_unique_hands):
  base_num = math.floor(requested_num_images / num_unique_hands)
  extra_probability = (requested_num_images % num_unique_hands) / num_unique_hands
  am = base_num
  if (random.random() < extra_probability):
      am += 1
  return am

# 2D Miniball
def minicircle(pts_):
  # pts = pts__.copy()
  pts = pts_[:, :2]
  min_x = pts[0][0]
  min_y = pts[0][1]

  max_x = pts[0][0]
  max_y = pts[0][1]

  # Find axis-aligned bounding box.
  # This is our "close enough" to the center of the smallest enclosing circle
  for pt in pts:
    min_x = min(min_x, pt[0])
    max_x = max(max_x, pt[0])

    min_y = min(min_y, pt[1])
    max_y = max(max_y, pt[1])
  
  cx = (min_x + max_x) / 2
  cy = (min_y + max_y) / 2

  c = np.array([cx, cy])

  # Find radius
  r = 0
  for pt in pts:
    r = max(r, np.linalg.norm(c-pt))

  return cx, cy, r





