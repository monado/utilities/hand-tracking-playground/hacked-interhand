import numpy as np
import torch
import cv2
from glob import glob
import os.path as osp
import os
import a_geometry as geo

from config import cfg

import smallestenclosingcircle

np.set_printoptions(precision=3, suppress=True)

# from utils.preprocessing import load_img, load_skeleton, get_bbox, process_bbox, augmentation, transform_input_to_output_space, trans_point2d
from utils.transforms import world2cam, cam2pixel, cam2pixel_depth
# from utils.vis import vis_keypoints, vis_3d_keypoints
import random
import ujson as json
import math
import pandas as pd

img_path = '../data/InterHand2.6M/images'
annot_path = '../data/InterHand2.6M/annotations'


def wobble_predictions_3d(predictions_3d):
  hand_size = np.linalg.norm(predictions_3d[0]-predictions_3d[9]) # distance from wrist to middle-pxm
  print(hand_size)


  move_all_fac = 0.3
  move_individual_fac = 0.02
  # (21, 3)
  move_all = np.ones((21, 3)) * np.random.normal(0, hand_size*move_all_fac, 3)

  move_individual = np.random.normal(0, hand_size*move_individual_fac, (21, 3))

  ret = predictions_3d + move_all + move_individual

  # print("before wobble\n", ret, "after wobble\n",  predictions_3d)

  return ret


def joint_lengths_3d(a):
  for idx, j in enumerate((predicted, joints_curr)):
      joint_coord = self.to_px_coord(self.cams[mode], capture_idx, cam_id, j,  hand_idx)


      moved = np.empty((21, 3))
      for i in range(21):
        moved[i] = geo.transformVecBy2x3(joint_coord[i], trans)
      joint_coord = moved

      for coord in joint_coord:
        cv2.circle(transformedHand, (int(coord[0]), int(coord[1])), 4, colors[idx])
    
      for finger in range(5):
        last_idx = 0
        for joint in range(4):
          coord = joint_coord[last_idx]
          prev = (int(coord[0]), int(coord[1]))

          curr_idx = 1 + finger*4 + joint
          
          coord = joint_coord[curr_idx]
          next = (int(coord[0]), int(coord[1]))
          last_idx = curr_idx;


          # cv2.circle(img, (int(coord[0]), int(coord[1])), 2, (1, 0, 1))

          cv2.line(transformedHand, prev, next, colors[idx], 1)



class InterDataset(torch.utils.data.Dataset):
  # root_dir = 
  def __init__(self):
    print("hi!");
    self.joints = {"train": [None]*26, "test": [None]*26, "val": [None]*26}
    self.cams = {}
    for mode in "train", "test", "val":
      with open(osp.join(annot_path, mode, 'InterHand2.6M_' + mode + '_camera.json')) as f:
        self.cams[mode] = json.load(f)
    
  def to_single_hand_order(self, joint_coord, hand_idx):
    new_joint_coord = np.empty((21, 3))
    if hand_idx == 0:
      joint_coord = joint_coord[0:21]
    else:
      joint_coord = joint_coord[21:]
    # Wrist.
    new_joint_coord[0] = joint_coord[20]
    for finger in range(5):
      offs = finger*4
      new_joint_coord[1+offs] = joint_coord[3+offs]
      new_joint_coord[2+offs] = joint_coord[2+offs]
      new_joint_coord[3+offs] = joint_coord[1+offs]
      new_joint_coord[4+offs] = joint_coord[0+offs]
    return new_joint_coord
  
  def fingertips_to_sphere_centers(self, joints):
    lerp_amount = 0.3
    for finger in range(5):
      offs_tip = 1 + finger*4 + 3
      offs_dst = 1 + finger*4 + 2

      tip = joints[offs_tip]
      dst = joints[offs_dst]


      joints[offs_tip] = (tip * (1.0-lerp_amount)) + (dst * lerp_amount)

  def predict_last_two(self, joint_n_2, joint_n_1):

    # return (joint_n_1*2) - joint_n_2 # ???? really? don't trust yourself.

    dir = joint_n_1 - joint_n_2

    out = joint_n_1 + dir


    return out
  
  

  def to_cam_coord(self, cameras, capture_id, cam, joints, hand_idx):
    campos, camrot = np.array(cameras[str(capture_id)]['campos'][str(cam)], dtype=np.float32), np.array(cameras[str(capture_id)]['camrot'][str(cam)], dtype=np.float32)
    focal, princpt = np.array(cameras[str(capture_id)]['focal'][str(cam)], dtype=np.float32), np.array(cameras[str(capture_id)]['princpt'][str(cam)], dtype=np.float32)
    # print(joints[str(capture_id)].keys())
    joint_world = np.array(joints['world_coord'], dtype=np.float32)
    joint_world = self.to_single_hand_order(joint_world, hand_idx)
    self.fingertips_to_sphere_centers(joint_world)
    joint_cam = world2cam(joint_world.transpose(1,0), camrot, campos.reshape(3,1)).transpose(1,0) 
    return joint_cam

  

  def to_px_coord(self, cameras, capture_id, cam, joint_cam, hand_idx):
    # campos, camrot = np.array(cameras[str(capture_id)]['campos'][str(cam)], dtype=np.float32), np.array(cameras[str(capture_id)]['camrot'][str(cam)], dtype=np.float32)
    focal, princpt = np.array(cameras[str(capture_id)]['focal'][str(cam)], dtype=np.float32), np.array(cameras[str(capture_id)]['princpt'][str(cam)], dtype=np.float32)
    # # print(joints[str(capture_id)].keys())
    # joint_world = np.array(joints['world_coord'], dtype=np.float32)
    # joint_world = self.to_single_hand_order(joint_world, hand_idx)
    # self.fingertips_to_sphere_centers(joint_world)
    # joint_cam = self.to_cam_coord(cameras, capture_id, cam, joints, hand_idx)
    joint_img = cam2pixel_depth(joint_cam, focal, princpt)

    # joint_coord = np.concatenate((joint_img, joint_cam[:,2,None]),1)

    # print("joint_img ->\n", joint_img)
    return joint_img

    
  def __getitem__(self, idx):
    file = f"/media/moses/TRAINDATA/hd/{idx}"

    a = pd.read_csv(file, delimiter=" ", quotechar="|", header=None)
    b = a.iloc[0]
    # hand_idx, mode, 
    # [0, 'test', 0, 'ROM01_No_Interaction_2_Hand', 400262, 20625, True]

    hand_idx = int(b[0])
    mode = str(b[1])
    capture_idx = int(b[2]) # 0 to 26 for train
    name = str(b[3])
    cam_id = int(b[4])
    frame_idx = int(b[5])
    frame_idx_n_1 = int(b[6])
    frame_idx_n_2 = int(b[7])

    # BAD

    
    # has_prior_hands = bool(b[6])

    # Lazy load
    if (self.joints[mode][capture_idx] == None):

      print("2")
      joint_name = osp.join(annot_path, mode, 'InterHand2.6M_' + mode + f'_joint_3d{capture_idx}.json')
      with open(joint_name) as f:
        joints = json.load(f)
      
      self.joints[mode][capture_idx] = joints

    img_file = osp.join(img_path, mode, f"Capture{capture_idx}", name, f"cam{cam_id}", f"image{frame_idx}.jpg")
    # if img_file == None:

    joints_curr = self.to_cam_coord(self.cams[mode], capture_idx, cam_id, self.joints[mode][capture_idx][str(frame_idx)], hand_idx)
    # print(joints_curr)

    img = cv2.imread(img_file, cv2.IMREAD_GRAYSCALE)
    img = img.astype(np.float32) * 1.0/255.0
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    # if 

    print(frame_idx_n_2, frame_idx_n_1, frame_idx)

    n_1 = self.to_cam_coord(self.cams[mode], capture_idx, cam_id, self.joints[mode][capture_idx][str(frame_idx_n_1)], hand_idx)
    n_2 = self.to_cam_coord(self.cams[mode], capture_idx, cam_id, self.joints[mode][capture_idx][str(frame_idx_n_2)], hand_idx)

    predicted = wobble_predictions_3d(self.predict_last_two(n_2, n_1))
    # predicted = self.predict_last_two(n_2, n_1)

    # colors = (1, 0, 1), (0, 1, 0)
    colors = (1, 0, 0), (0, 1, 0), (0, 0, 1), (1, 0, 1)
    
    
    

    rot = random.uniform(-math.pi, math.pi)

    x_axis = np.float32([math.cos(rot), math.sin(rot)])
    y_axis = np.float32([-math.sin(rot), math.cos(rot)])

    # Just rotation
    tl = (0, 0)
    tr = (0, 1)
    bl = (1, 0)
    
    tl_o = (0, 0)
    tr_o = x_axis
    bl_o = y_axis

    src_tri = np.float32((tl, tr, bl))
    dst_tri = np.float32((tl_o, tr_o, bl_o))

    trans_rot = cv2.getAffineTransform(src_tri, dst_tri)

    in_rotated = np.empty((21, 3))

    predicted_px = self.to_px_coord(self.cams[mode], capture_idx, cam_id, predicted,  hand_idx)
    gt_px = self.to_px_coord(self.cams[mode], capture_idx, cam_id, joints_curr,  hand_idx)

    # vectorize!
    for i in range(21):
      in_rotated[i] = geo.transformVecBy2x3(predicted_px[i], trans_rot)
    
    x,y,r = geo.minicircle(in_rotated)

    radius_function = [(1.2, 1.0), (1.4, 1.0), (1.6, 0.8), (1.8, 0.6), (2.0, 0.5), (2.2, 0.0)] # one at the end doesn't matter.
    radius_function = [(1.2, 1.0), (1.4, 0.9), (1.6, 0.8), (1.8, 0.5), (2.0, 0.2), (2.4, 0.0)]

    the_sum = 0
    for guy in radius_function[:-1]:
        # print(guy[1])
        the_sum += guy[1]


    r_mul = geo.arbitrary_density(random.uniform(0, the_sum), radius_function)

    final_r = r * r_mul # NO.
    sz = 512
    center = np.array((x, y))

    center += np.random.normal(0, 0.03*r, (2))

    is_right = (hand_idx == 1)


    # Translation and scale, on top of the rotation we've already done.
    rot = 0 # :)
    x_axis = np.float32([math.cos(rot), math.sin(rot)])*final_r
    if is_right:
        x_axis *= -1
    y_axis = np.float32([-math.sin(rot), math.cos(rot)])*final_r



    tl = center - x_axis - y_axis
    tr = center + x_axis - y_axis
    bl = center - x_axis + y_axis

    tl_o = (0, 0)
    tr_o = (sz, 0)
    bl_o = (0, sz)

    src_tri = np.float32((tl, tr, bl))
    dst_tri = np.float32((tl_o, tr_o, bl_o))

    trans_movescale = cv2.getAffineTransform(src_tri, dst_tri)

    trans_rot_3x3 = np.eye(3)
    trans_movescale_3x3 = np.eye(3)

    trans_rot_3x3[0:2, 0:3] = trans_rot
    trans_movescale_3x3[0:2, 0:3] = trans_movescale

    trans = np.dot(trans_movescale_3x3, trans_rot_3x3)
    trans = trans[0:2]




    transformedHand = cv2.warpAffine(img, trans, (sz, sz))

    print(joints_curr)

    print(self.to_px_coord(self.cams[mode], capture_idx, cam_id, joints_curr,  hand_idx))

    for idx, j in enumerate((predicted, joints_curr)):
      joint_coord = self.to_px_coord(self.cams[mode], capture_idx, cam_id, j,  hand_idx)


      moved = np.empty((21, 3))
      for i in range(21):
        moved[i] = geo.transformVecBy2x3(joint_coord[i], trans)
      joint_coord = moved

      for coord in joint_coord:
        cv2.circle(transformedHand, (int(coord[0]), int(coord[1])), 4, colors[idx])
    
      for finger in range(5):
        last_idx = 0
        for joint in range(4):
          coord = joint_coord[last_idx]
          prev = (int(coord[0]), int(coord[1]))

          curr_idx = 1 + finger*4 + joint
          
          coord = joint_coord[curr_idx]
          next = (int(coord[0]), int(coord[1]))
          last_idx = curr_idx;


          # cv2.circle(img, (int(coord[0]), int(coord[1])), 2, (1, 0, 1))

          cv2.line(transformedHand, prev, next, colors[idx], 1)



    # print(x,y,r)

    cv2.imshow("hi", transformedHand)
    # cv2.waitKey(0)


    # print(a.iloc[0])
    return None


d = InterDataset()


# for e in d:
#   print(e)

for i in range(145, 500):
  # print(d[i])
  r = d[i]
  print(i)
  cv2.waitKey(0)
  # print("")

# d[0]
# d[1]
# d[7]
